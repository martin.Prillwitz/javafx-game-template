package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    public InputHandler(){};

    public InputHandler(Model model) {
        this.model = model;
    }


    public void onKeyPressed(KeyCode keycode) {

        if(keycode==KeyCode.ENTER){
            model.setLetsgo(true);
        }

        if (keycode == KeyCode.UP) {
            model.getKopf().setMoveUp(true);

            model.getKopf().setMoveDown(false);
            model.getKopf().setMoveLeft(false);
            model.getKopf().setMoveRight(false);
        }
        if (keycode == KeyCode.DOWN) {
            model.getKopf().setMoveDown(true);

            model.getKopf().setMoveLeft(false);
            model.getKopf().setMoveRight(false);
            model.getKopf().setMoveUp(false);
        }
        if (keycode == KeyCode.LEFT) {
            model.getKopf().setMoveLeft(true);

            model.getKopf().setMoveRight(false);
            model.getKopf().setMoveUp(false);
            model.getKopf().setMoveDown(false);
        }
        if (keycode == KeyCode.RIGHT) {
            model.getKopf().setMoveRight(true);

            model.getKopf().setMoveLeft(false);
            model.getKopf().setMoveUp(false);
            model.getKopf().setMoveDown(false);

        }
    }

    /*public void onKeyPressed(KeyCode keycode) {

        if (keycode == KeyCode.UP) {

            model.getKopf().move(0, -20);
        }
        if (keycode == KeyCode.DOWN) {
            model.getKopf().move(0, 20);
        }
        if (keycode == KeyCode.LEFT) {
            model.getKopf().move(-20, 0);
        }
        if (keycode == KeyCode.RIGHT) {
            model.getKopf().move(20, 0);

        }
    }*/

}
