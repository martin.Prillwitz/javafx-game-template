package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Graphics {

    Image hintergrund;
    Image holz;
    Image pizza;
    Image titel;
    Image apfel;

    private GraphicsContext gc;
    private Model model;


    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
        try{
            hintergrund = new Image(new FileInputStream("src/de/awacademy/pictures/Rasen.png"));
            holz = new Image(new FileInputStream("src/de/awacademy/pictures/Holz.png"));
            pizza = new Image(new FileInputStream("src/de/awacademy/pictures/Pizza.png"));
            titel = new Image(new FileInputStream("src/de/awacademy/pictures/Titel2.png"));
            apfel = new Image(new FileInputStream("src/de/awacademy/pictures/apple.png"));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
    }

    public void draw() {
        gc.drawImage(titel,0,0,500,500);

        if (model.isLetsgo()) {


            gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);//Methode welche den kompletten Spiel Bereich immer wieder resetet
            gc.drawImage(hintergrund,0,0,500,400);
            gc.setFill(Color.RED);
            gc.fillRect(model.getKopf().getX(), model.getKopf().getY(), model.getKopf().getW(), model.getKopf().getH());
            for (int i = 0; i < model.getTailpieces().size(); i++) {
                gc.setFill(Color.GREEN);
                gc.fillRect(model.getTailpieces().get(i).getX(), model.getTailpieces().get(i).getY(), model.getTailpieces().get(i).getW(), model.getTailpieces().get(i).getH());
            }
            gc.drawImage(pizza, model.getPizza().getX(), model.getPizza().getY(), model.getPizza().getW(), model.getPizza().getH());
            gc.drawImage(holz,0,400,500,100);
            gc.setFill(Color.WHITE);
            gc.setFont(new Font("", 40));
            gc.fillText("Pizza Score: " + model.getCounter(), 120, 450);

            for (int i = 0; i < model.getGiftigeListe().size(); i++) {

                gc.drawImage(apfel, model.getGiftigeListe().get(i).getX(), model.getGiftigeListe().get(i).getY(), model.getGiftigeListe().get(i).getW(), model.getGiftigeListe().get(i).getH());

            }


        }


    }


}

