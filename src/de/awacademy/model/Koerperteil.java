package de.awacademy.model;

public class Koerperteil {

    private int x;
    private int y;
    private int h;
    private int w;
    private boolean wait = true;

    public Koerperteil(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 20;
        this.w = 20;
    }

    public boolean isWait() {
        return wait;
    }

    public void setWait(boolean wait) {
        this.wait = wait;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
