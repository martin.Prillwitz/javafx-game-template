package de.awacademy.model;

import java.util.Random;

public class Pizza {

    private int x;
    private int y;
    private int h;
    private int w;

    public Pizza() {
        Random zufallszahl = new Random();
        this.x = zufallszahl.nextInt(420);
        this.y = zufallszahl.nextInt(320);
        this.h = 50;
        this.w = 50;
    }


    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
