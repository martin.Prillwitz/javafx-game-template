package de.awacademy.model;

import java.util.ArrayList;
import java.util.Random;

public class Model {

    private Kopf kopf;
    private Pizza pizza;
    private int counter;
    private boolean letsgo = false;

    public final double WIDTH = 500;
    public final double HEIGHT = 500;

    public Model() {
        this.kopf = new Kopf(0, 0);
        this.pizza = new Pizza();
    }

    public Kopf getKopf() {
        return kopf;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public ArrayList<GiftigerApfel> getGiftigeListe() {
        return giftigeListe;
    }
    //Arraylisten um die giftigen Apfel und die Körperteile der Schlange zu speichern
    ArrayList<Koerperteil> tailpieces = new ArrayList<>();
    ArrayList<GiftigerApfel> giftigeListe = new ArrayList<>();

    //wenn die Schlange Pizza isst:
    //erzeuge
    public void kollision() {
        int differenzX = kopf.getX() - pizza.getX();
        int differenzY = kopf.getY() - pizza.getY();

        if (Math.abs(differenzX) < 30 && Math.abs(differenzY) < 30) {
            Random zufallszahl = new Random();
            pizza.setX(zufallszahl.nextInt(460));
            pizza.setY(zufallszahl.nextInt(360));
            counter++;
            giftigeListe.add(new GiftigerApfel());

            if (tailpieces.size() < 1) {
                tailpieces.add(new Koerperteil(kopf.getX(), kopf.getY()));
            } else {
                for (int i = 0; i < 5; i++) {
                    tailpieces.add(new Koerperteil(tailpieces.get(tailpieces.size() - 1).getX(), tailpieces.get(tailpieces.size() - 1).getY()));
                }
            }
        }
    }

    public void hitWall() {

        if (kopf.getX() < 0 || kopf.getX() > 500 || kopf.getY() > 400 || kopf.getY() < 0) {
            kopf.setX(200);
            kopf.setY(200);
            tailpieces.clear();
            giftigeListe.clear();
            counter = 0;
        }
    }

    public void hitGiftigerApfel() {
        for (GiftigerApfel check : giftigeListe) {
            int differenzX = kopf.getX() - check.getX();
            int differenzY = kopf.getY() - check.getY();

            if (Math.abs(differenzX) < 15 && Math.abs(differenzY) < 15) {
                kopf.setX(200);
                kopf.setY(200);
                giftigeListe.clear();
                tailpieces.clear();
                counter = 0;
            }
        }
    }

        /*
        for (int i = 20; i < tailpieces.size(); i++) {
            int differenzX=kopf.getX()-tailpieces.get(i).getX();
            int differenzY=kopf.getY()-tailpieces.get(i).getY();
            if (differenzX<=1 && differenzY<=1){
                kopf.setX(200);
                kopf.setY(200);
                tailpieces.clear();
            }
        }*/


    public void movementTail() {
        if (tailpieces.size() > 1) {
            //Bewege die einzelnen Körperteile so wie ihre Vorgängerkörperteile
            for (int i = tailpieces.size() - 1; i >= 1; i--) {
                tailpieces.get(i).setX(tailpieces.get(i - 1).getX());
                tailpieces.get(i).setY(tailpieces.get(i - 1).getY());

            }
        }//Bewege das oberste Körperteil so wie den Kopf
        if (tailpieces.size() > 0) {
            tailpieces.get(0).setX(kopf.getX());
            tailpieces.get(0).setY(kopf.getY());
        }
    }

    public ArrayList<Koerperteil> getTailpieces() {
        return tailpieces;
    }

    public int getCounter() {
        return counter;
    }

    public boolean isLetsgo() {
        return letsgo;
    }

    public void setLetsgo(boolean letsgo) {
        this.letsgo = letsgo;
    }
}
