package de.awacademy.model;

import java.util.Random;

public class GiftigerApfel {

    private int x;
    private int y;
    private int h;
    private int w;

    public GiftigerApfel() {
        Random zufallszahl = new Random();
        this.x = zufallszahl.nextInt(480);
        this.y = zufallszahl.nextInt(380);
        this.h = 30;
        this.w = 30;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }
}
