package de.awacademy.model;

public class Kopf {

    private int x;
    private int y;
    private int h;
    private int w;

    private boolean moveUp = false;
    private boolean moveDown = false;
    private boolean moveLeft = false;
    private boolean moveRight = false;


    public Kopf(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 20;
        this.w = 20;
    }

    public void movement(int bewegung) {
        if (moveUp) {
            this.y += -bewegung;
        }
        if (moveDown) {
            this.y += bewegung;
        }
        if (moveRight) {
            this.x += bewegung;
        }
        if (moveLeft) {
            this.x += -bewegung;
        }
    }

    public void move(int dx, int dy) {

        this.x += dx;
        this.y += dy;

    }

    public boolean isMoveUp() {
        return moveUp;
    }

    public boolean isMoveDown() {
        return moveDown;
    }

    public boolean isMoveLeft() {
        return moveLeft;
    }

    public boolean isMoveRight() {
        return moveRight;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


    public void setMoveUp(boolean moveUp) {
        this.moveUp = moveUp;
    }

    public void setMoveDown(boolean moveDown) {
        this.moveDown = moveDown;
    }

    public void setMoveLeft(boolean moveLeft) {
        this.moveLeft = moveLeft;
    }

    public void setMoveRight(boolean moveRight) {
        this.moveRight = moveRight;
    }
}
