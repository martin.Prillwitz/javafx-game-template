package de.awacademy;

import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;




    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    //Methoden welche immer wieder augerufen werden soll

    @Override
    public void handle(long nowNano) {
        model.getKopf().movement(3);
        graphics.draw();
        model.kollision();
        model.movementTail();
        model.hitWall();
        model.hitGiftigerApfel();


    }

}
